import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[]args){
		Scanner scan = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		
		for (int i = 0; i < appliances.length; i++){
			appliances[i] = new Refrigerator();
			appliances[i].freezer = scan.nextBoolean();
			appliances[i].temperature =scan.nextInt();
			appliances[i].noShelf = scan.nextInt();
			
			System.out.println();
		}
		
		System.out.println(appliances[3].freezer + ", " + appliances[3].temperature + ", "+appliances[3].noShelf);
		
		appliances[0].ifFreezer();
		appliances[0].checkTemperature();
	}
}